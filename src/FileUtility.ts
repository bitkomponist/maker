import { existsSync, promises as fs } from 'fs';
import { join, dirname } from 'path';
import { Json } from './Json';
import get from 'lodash.get';
import set from 'lodash.set';
import { PropertyPath } from 'lodash';

export type FileDataTransformer = (serialized: string) => unknown;
export type FileDataSerializer = (transformed: unknown) => string;

export class File {
  protected path = '';
  protected transform?: (contents: string) => unknown;
  protected serialize?: (transformed: unknown) => string;
  public data?: unknown = '';

  constructor(
    path: string,
    transform?: FileDataTransformer,
    serialize?: FileDataSerializer,
  ) {
    Object.assign(this, { path, transform, serialize });
  }

  async load() {
    const { transform, path } = this;
    if (existsSync(path)) {
      this.data = await fs.readFile(path,{encoding:'utf8'});
    }

    if (transform && typeof this.data === 'string') {
      this.data = transform(this.data);
    }

    return this;
  }

  async save() {
    const { path, data, serialize = String } = this;
    if (!existsSync(path)) {
      await this.ensurePath();
    }
    await fs.writeFile(path, serialize(data),{encoding:'utf8'});
    return this;
  }

  ensurePath() {
    return fs.mkdir(dirname(this.path), { recursive: true });
  }
}

export class JsonFile extends File {
  public data: Json = {};
  constructor(path: string) {
    super(
      path,
      (str: string): Json => {
        if (str === '') return {};
        try {
          return JSON.parse(str);
        } catch (e) {
          console.warn(e);
          return {};
        }
      },
      (obj: unknown): string => JSON.stringify(obj, null, 4),
    );
  }
  async load() {
    await super.load();
    if (!this.data) {
      this.data = {};
    }
    return this;
  }

  get(path: PropertyPath, defaultValue?: unknown) {
    if (!this.data) {
      this.data = {};
    }
    return get(this.data, path, defaultValue);
  }

  set(path: PropertyPath, value: unknown) {
    if (!this.data) {
      this.data = {};
    }
    return set<Json>(this.data as Record<string, Json>, path, value);
  }
}

export class ConfigFile extends JsonFile {
  constructor(path?: string) {
    if (!path) {
      path = join(process.cwd(), '.maker', 'config.json');
    }
    super(path);
  }
}

export function getFile(
  path: string,
  transform?: FileDataTransformer,
  serialize?: FileDataSerializer,
) {
  return new File(path, transform, serialize).load();
}

export function getJsonFile(path: string) {
  return new JsonFile(path).load();
}

export function getConfigFile(path?: string) {
  return new ConfigFile(path).load();
}

export interface FileUtility {
  getFile(
    path: string,
    transform?: FileDataTransformer,
    serialize?: FileDataSerializer,
  ): Promise<File>;
  getJsonFile(path: string): Promise<JsonFile>;
  getConfigFile(path?: string): Promise<ConfigFile>;
}

export default function fileUtility(): FileUtility {
  return {
    getFile,
    getJsonFile,
    getConfigFile,
  };
}
