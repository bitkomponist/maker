import { ChildProcess, spawn as nativeSpawn } from 'child_process';

export type SpawnedCallback = (c: ChildProcess) => void;

export type SpawnArguments = readonly string[];

export function spawn(
  cmd: string,
  args: SpawnArguments = [],
  options: Record<string, unknown> = {},
  createdCallback?: SpawnedCallback,
): Promise<string> {
  options = {
    encoding: 'utf8',
    maxBuffer: 1024 * 1024 * 100,
    ...(options || {}),
  };

  return new Promise((resolve, reject) => {
    const childProcess = nativeSpawn(cmd, args, options);
    if (createdCallback) {
      createdCallback(childProcess);
    }
    let result = '';
    let err = '';
    const onOutData = (chunk: string) => (result += chunk);
    const onErrorData = (chunk: string) => (err += chunk);
    const stdout = childProcess.stdout || process.stdout;
    const stderr = childProcess.stderr || process.stderr;
    const stdin = childProcess.stdin || process.stdin;

    stdout.on('data', onOutData);
    stderr.on('data', onErrorData);

    childProcess.on('error', (error) => {
      stdout.off('data', onOutData);
      stderr.off('data', onErrorData);
      reject(error);
    });

    childProcess.on('close', (code) => {
      stdout.off('data', onOutData);
      stderr.off('data', onErrorData);
      stdin.end();
      if (code) {
        reject(err.trim());
      } else {
        resolve(result.trim());
      }
    });

    // childProcess.on('exit', (code) => {
    //   stdout.off('data', onOutData);
    //   stderr.off('data', onErrorData);
    //   if (code) {
    //     reject(err.trim());
    //   } else {
    //     resolve(result.trim());
    //   }
    // });

    /*
      const spawn = require('child_process').spawn;
      const bat = spawn('ssh', ['user@host']);

      bat.stdout.on('data', function(d) {
        console.log(d.toString());
      });
      bat.stderr.on('data', function(d) {
        console.log(d.toString());
      });

      bat.on('error', function(error) {
        console.log('child err\'d with : ' + error);
      });
      bat.on('close', function(code, signal) {
        console.log('child exited with code: ' + code + ' and signal: ' + signal);
      });

      process.stdin.on('readable', () => {
        var chunk = process.stdin.read();
        if (chunk !== null) {
          bat.stdin.write(chunk);
        }
      });
    */
  });
}

export function spawnWithIO(
  cmd: string,
  args: SpawnArguments = [],
  options = {},
  createdCallback?: SpawnedCallback,
) {
  return spawn(
    cmd,
    args,
    { stdio: 'inherit', env: process.env, ...options },
    createdCallback,
  );
}

export function exec(
  cmd: string,
  options = {},
  createdCallback?: SpawnedCallback,
) {
  const args = cmd.split(' ');
  const file = args.shift();
  if (!file) {
    throw new Error(`invalid command`);
  }
  return spawn(file, args, options, createdCallback);
}

export function execWithIO(
  cmd: string,
  options = {},
  createdCallback?: SpawnedCallback,
) {
  return exec(
    cmd,
    { stdio: 'inherit', env: process.env, ...options },
    createdCallback,
  );
}

export interface ChildProcessUtility {
  spawn(
    cmd: string,
    args: SpawnArguments,
    options?: Record<string, unknown>,
    createdCallback?: SpawnedCallback,
  ): Promise<string>;
  spawnWithIO(
    cmd: string,
    args: SpawnArguments,
    options?: Record<string, unknown>,
    createdCallback?: SpawnedCallback,
  ): Promise<string>;
  exec(
    cmd: string,
    options?: Record<string, unknown>,
    createdCallback?: SpawnedCallback,
  ): Promise<string>;
  execWithIO(
    cmd: string,
    options?: Record<string, unknown>,
    createdCallback?: SpawnedCallback,
  ): Promise<string>;
}

export default function childProcessUtility(): ChildProcessUtility {
  return {
    spawn,
    spawnWithIO,
    exec,
    execWithIO,
  };
}
