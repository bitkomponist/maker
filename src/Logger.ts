interface ConsoleStyles {
  [x: string]: string;
}
const CONSOLE_STYLES: ConsoleStyles = {
  reset: '\x1b[0m',
  bright: '\x1b[1m',
  dim: '\x1b[2m',
  underscore: '\x1b[4m',
  blink: '\x1b[5m',
  reverse: '\x1b[7m',
  hidden: '\x1b[8m',

  black: '\x1b[30m',
  red: '\x1b[31m',
  green: '\x1b[32m',
  yellow: '\x1b[33m',
  blue: '\x1b[34m',
  magenta: '\x1b[35m',
  cyan: '\x1b[36m',
  white: '\x1b[37m',

  'bg-black': '\x1b[40m',
  'bg-red': '\x1b[41m',
  'bg-green': '\x1b[42m',
  'bg-yellow': '\x1b[43m',
  'bg-blue': '\x1b[44m',
  'bg-magenta': '\x1b[45m',
  'bg-cyan': '\x1b[46m',
  'bg-white': '\x1b[47m',
};

function getConsoleStyle(...styles: string[]): string {
  return [
    ...styles.map((key) => CONSOLE_STYLES[key]).filter(Boolean),
    '%s',
    CONSOLE_STYLES.reset,
  ].join('');
}

function log(message: unknown, ...styles: string[]) {
  if (!styles.length) {
    console.log(message);
  } else {
    console.log(getConsoleStyle(...styles), message);
  }
}

export function style(message: string, ...styles: string[]): string {
  return getConsoleStyle(...styles).replace('%s', message);
}

export function info(message: unknown, ...styles: string[]): void {
  log(message, 'cyan', ...styles);
}

export function warn(message: unknown, ...styles: string[]): void {
  log(message, 'yellow', ...styles);
}

export function error(message: unknown, ...styles: string[]): void {
  log(message, 'red', ...styles);
}

export function success(message: unknown, ...styles: string[]): void {
  log(message, 'green', ...styles);
}

export interface Logger {
  style(message: string, ...styles: string[]): string;
  log(message: unknown, ...styles: string[]): void;
  info(message: unknown, ...styles: string[]): void;
  warn(message: unknown, ...styles: string[]): void;
  error(message: unknown, ...styles: string[]): void;
  success(message: unknown, ...styles: string[]): void;
}

export default function logger(): Logger {
  return {
    log,

    style,

    info,

    warn,

    error,

    success,
  };
}
