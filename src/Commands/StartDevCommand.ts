import commander from 'commander';
import { Cli } from '../Cli';

export default (programm: commander.Command, cli: Cli) => {
  const { spawnWithIO, spawn } = cli;
  programm
    .command('start:dev')
    .description('start the local development server')
    .option('-t, --tools', 'also launch tools (phpmyadmin, mailhog)', true)
    .action(async (_arg0, { tools }) => {
      await spawnWithIO('ddev', ['start']);
      await spawn('ddev', ['launch', '/']);
      await spawn('ddev', ['launch', '/admin']);
      if (tools) {
        await spawn('ddev', ['launch', '-p']);
        await spawn('ddev', ['launch', '-m']);
      }
    });
};
