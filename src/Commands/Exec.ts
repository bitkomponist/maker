import commander from 'commander';
import { Cli } from '../Cli';

export default (programm: commander.Command, cli: Cli) => {
  programm
    .command('exec <argsString>')
    .usage('maker exec "ls -lh"')
    .description(
      'exec commands, for dev purposes',
    )
    .action(async (argsString) => {
      const [cmdFile,...cmdArgs] = argsString.split(' ');
      await cli.spawnWithIO(cmdFile, cmdArgs);
    });
};
