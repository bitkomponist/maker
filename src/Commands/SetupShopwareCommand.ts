import { existsSync } from 'fs';
import { join, basename } from 'path';
import commander from 'commander';
import { Cli } from '../Cli';
import prompts from 'prompts';
import { ConfigFile } from '../FileUtility';
import { SpawnArguments } from '../ChildProcessUtility';

export default (programm: commander.Command, cli: Cli) => {
  let config: ConfigFile;
  const wd = process.cwd();

  function ensureDependencies() {
    return cli.ensureDependencies({
      git: '2.25.0',
      ddev: '1.16.5',
      docker: '20.10.0',
      node: '14.4.0',
    });
  }

  async function initConfig() {
    config = await cli.getConfigFile();

    const {
      repository,
      branch,
      applicationDirectory,
      sourceDirectory,
      projectName,
    } = await prompts([
      {
        name: 'repository',
        type: 'text',
        message: 'Shopware Boilerplate repository',
        initial: config.get('boilerplate.repository','https://github.com/shopware/production'),
      },
      {
        name: 'branch',
        type: 'text',
        message: 'Which Shopware branch to use',
        initial: config.get('boilerplate.branch','6.2'),
      },
      {
        name: 'applicationDirectory',
        type: 'text',
        message: 'Application Directory',
        initial: config.get('applicationDirectory','app'),
      },
      {
        name: 'sourceDirectory',
        type: 'text',
        message: 'Source Directory',
        initial: config.get('sourceDirectory','src'),
      },
      {
        name: 'projectName',
        type: 'text',
        message: 'Project Name',
        initial: config.get('projectName',basename(wd)),
      },
    ]);

    const appUrl = `https://${projectName}.ddev.site`;

    Object.assign(config.data, {
      projectName,
      projectType: 'shopware6',
      boilerplate: {
        repository,
        branch,
      },
      applicationDirectory,
      sourceDirectory,
      appUrl,
      // appSecret:await cli.spawn('openssl',['rand', '-hex', 32]),
      // appInstanceId:await cli.spawn('openssl',['rand', '-hex', 32])
    });

    await config.save();
  }

  async function initBoilerplate() {
    const applicationDirectory = config.get('applicationDirectory');
    const repository = config.get('boilerplate.repository');
    const branch = config.get('boilerplate.branch');
    const sourceDirectory = config.get('sourceDirectory');

    cli.info('Download and configure Shopware boilerplate');

    if (!existsSync(join(wd, applicationDirectory))) {
      await cli.spawnWithIO('git', [
        'clone',
        `--branch=${branch}`,
        repository,
        applicationDirectory,
      ]);
    } else {
      cli.warn(`boilerplate already exists, skipping...`);
    }

    const composerFile = await cli.getJsonFile(
      join(applicationDirectory, 'composer.json'),
    );

    if (!composerFile.get('repositories')) {
      composerFile.set('repositories', []);
    }

    await cli.spawnWithIO('mkdir', [
      '-p',
      join(sourceDirectory, 'static-plugins'),
    ]);

    const repoLinkUrl = join('../', sourceDirectory, 'static-plugins/*');
    if (
      !composerFile
        .get('repositories')
        .some(
          ({ url, type }: { url: string; type: string }) =>
            type === 'path' && url === repoLinkUrl,
        )
    ) {
      composerFile.get('repositories').push({
        type: 'path',
        url: repoLinkUrl,
        options: {
          symlink: true,
        },
      });
      await composerFile.save();
    }
  }

  async function initDDEV() {
    if(existsSync(join(wd,'.ddev/config.yaml'))){
      cli.warn(`ddev already configured, skipping...`);
      return;
    }

    const applicationDirectory = config.get('applicationDirectory');
    const projectName = config.get('projectName');
    const projectType = config.get('projectType');

    cli.info('Initialize DDEV Environment');

    await cli.spawnWithIO('ddev', [
      'config',
      `--project-type=${projectType}`,
      `--docroot=${applicationDirectory}/public`,
      `--project-name=${projectName}`,
    ]);

    await cli.spawnWithIO('ddev', ['start']);
  }

  function getWebContainerWD() {
    if(!config || !config.get('applicationDirectory')){
      throw new Error('invalid config, missing applicationDirectory');
    }
    return `/var/www/html/${config.get('applicationDirectory')}`;
  }

  function composer(...args: SpawnArguments) {
    return cli.spawnWithIO('ddev', [
      'exec',
      'composer',
      `--working-dir=${getWebContainerWD()}`,
      ...args,
    ]);
  }

  function shopwareConsole(...args: SpawnArguments) {
    return cli.spawnWithIO('ddev', [
      'exec',
      `--dir=${getWebContainerWD()}`,
      'bin/console',
      ...args,
    ]);
  }

  async function initShopware() {
    const applicationDirectory = config.get('applicationDirectory');
    const appUrl = config.get('appUrl');

    await composer('install');

    await shopwareConsole(
      'system:setup',
      '-n',
      `--database-url=mysql://db:db@db:3306/db`,
      `--env=dev`,
    );

    const envFile = await cli.getFile(join(wd, applicationDirectory, '.env'));

    envFile.data += '\n# maker-generated\n';
    envFile.data += Object.entries({
      APP_ENV: 'dev',
      APP_URL: appUrl,
      MAILER_URL: 'smtp://localhost:1025',
    })
      .map(([key, value]) => `${key}="${value}"`)
      .join('\n');

    await envFile.save();

    await shopwareConsole(
      'system:install',
      '--create-database',
      '--basic-setup',
    );
  }

  programm
    .command('setup:shopware')
    .description(
      'Download and initialize all dependencies necessary to develop the project',
    )
    .action(async () => {
      await ensureDependencies();

      await initConfig();

      await initBoilerplate();

      await initDDEV();

      await initShopware();

      await cli.spawnWithIO('ddev', ['launch', '/admin']);
    });
};
