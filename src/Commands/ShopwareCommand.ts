import commander from 'commander';
import { Cli } from '../Cli';

export default (programm: commander.Command, cli: Cli) => {
  programm
    .command('shopware [...shopware-args]')
    .description(
      'invoke shopware cli in the configured application directory on the web container',
    )
    .action(async (_arg0, { args }) => {
      const config = await cli.getConfigFile();
      const wd = `/var/www/html/${config.get('applicationDirectory', '')}`;

      await cli.spawnWithIO('ddev', [
        'exec',
        `--dir=${wd}`,
        'bin/console',
        ...args,
      ]);
    });
};
