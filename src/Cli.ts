import childProcessUtility, {
  SpawnArguments,
  ChildProcessUtility,
} from './ChildProcessUtility';
import logger, { Logger } from './Logger';
import fileUtility, { FileUtility } from './FileUtility';
import versionUtility, { VersionUtility } from './VersionUtility';
import commandsRegistry, {
  CommandsRegistry,
  Commands,
} from './CommandsRegistry';
import commander from 'commander';

export interface Cli
  extends Logger,
    FileUtility,
    ChildProcessUtility,
    VersionUtility,
    CommandsRegistry {
  run(...args: SpawnArguments): Promise<unknown>;
}

export default async function cli(
  includePaths?: string | string[],
  commands: Commands = [],
): Promise<Cli> {
  const registry = await commandsRegistry(includePaths, commands);
  const programm = new commander.Command();
  const instance = {
    ...registry,
    ...logger(),
    ...childProcessUtility(),
    ...fileUtility(),
    ...versionUtility(),
    async run(...argv: SpawnArguments): Promise<Cli> {
      await programm.parseAsync([...argv]);
      return instance;
    },
  };

  for (const commandFactory of registry.commands) {
    commandFactory(programm, instance);
  }

  return instance;
}
