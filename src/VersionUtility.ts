import compareVersionsStrings from 'compare-versions';
import { spawn, SpawnArguments } from './ChildProcessUtility';

export async function parseVersion(
  cmd: string,
  flag = '--version',
  ...args: SpawnArguments
): Promise<boolean | string> {
  let info: string;
  try {
    info = await spawn(cmd, [flag, ...args]);
  } catch (e) {
    return false;
  }

  const [version = false] =
    info.match(/(?<=(v|version)\s*)(\d+(\.\d+)*)/gi) || [];
  return version;
}

export async function compareVersions(
  cmd: string,
  targetVersion: string,
  flag = '--version',
  ...args: SpawnArguments
) {
  const installedVersion = await parseVersion(cmd, flag, ...args);
  if (installedVersion === false) return -1;
  return compareVersionsStrings(String(installedVersion), targetVersion);
}

export async function ensureDependencies(dependencies: Record<string, string>) {
  for (const [file, minVersion] of Object.entries(dependencies)) {
    const comparison = await compareVersions(file, minVersion);
    if (comparison < 0) {
      throw new Error(
        `${file} is not installed or older than min-version ${minVersion}. please install/update först and retry`,
      );
    }
  }
}

export interface VersionUtility {
  parseVersion(
    cmd: string,
    flag?: string,
    ...args: SpawnArguments
  ): Promise<boolean | string>;
  compareVersions(
    cmd: string,
    targetVersion: string,
    flag?: string,
    ...args: SpawnArguments
  ): Promise<0 | 1 | -1>;
  ensureDependencies(dependencies: Record<string, string>): void;
}

export default function versionUtility(): VersionUtility {
  return {
    parseVersion,
    compareVersions,
    ensureDependencies,
  };
}
