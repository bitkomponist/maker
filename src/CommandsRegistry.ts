import { join, sep } from 'path';
import glob from 'fast-glob';
import { Cli } from './Cli';
import commander from 'commander';

export const DEFAULT_INCLUDE_PATH: string[] = [
  join(__dirname, 'commands/*.js'),
  '.maker/commands/*.js',
];

export type CommandFactory = (program: commander.Command, cli: Cli) => unknown;

export type Commands = CommandFactory[];

export interface CommandsRegistry {
  commands: Commands;
  load(includePaths: string | string[]): Promise<CommandsRegistry>;
}

export default async function commandsRegistry(
  includePaths: string | string[] = DEFAULT_INCLUDE_PATH,
  commands: Commands = [],
): Promise<CommandsRegistry> {
  async function load(this: CommandsRegistry, includePaths: string | string[]) {
    for (let file of await glob(includePaths)) {
      if (!file.startsWith(sep)) {
        file = join(process.cwd(), file);
      }

      const { default: commandFactory } = await import(file);

      if (typeof commandFactory !== 'function')
        throw new Error(`invalid CommandFactory ${file}`);

      commands.push(commandFactory);
    }

    return this;
  }

  const registry = {
    commands,
    load,
  };

  return registry.load(includePaths);
}
