import Cli from './Cli';
interface CommandOption {
    description?: string;
    short?: string;
    type?: (input: string | number) => unknown;
    default: boolean;
}
interface CommandOptions {
    [x: string]: CommandOption;
}
export default abstract class Command {
    static identifier?: string;
    static description?: string;
    static man?: string;
    static options?: CommandOptions;
    protected cli: Cli;
    constructor(cli: Cli);
    abstract run(...args: readonly string[]): Promise<string>;
}
export {};
