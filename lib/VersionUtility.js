"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ensureDependencies = exports.compareVersions = exports.parseVersion = void 0;
const compare_versions_1 = __importDefault(require("compare-versions"));
const ChildProcessUtility_1 = require("./ChildProcessUtility");
async function parseVersion(cmd, flag = '--version', ...args) {
    let info;
    try {
        info = await ChildProcessUtility_1.spawn(cmd, [flag, ...args]);
    }
    catch (e) {
        return false;
    }
    const [version = false] = info.match(/(?<=(v|version)\s*)(\d+(\.\d+)*)/gi) || [];
    return version;
}
exports.parseVersion = parseVersion;
async function compareVersions(cmd, targetVersion, flag = '--version', ...args) {
    const installedVersion = await parseVersion(cmd, flag, ...args);
    if (installedVersion === false)
        return -1;
    return compare_versions_1.default(String(installedVersion), targetVersion);
}
exports.compareVersions = compareVersions;
async function ensureDependencies(dependencies) {
    for (const [file, minVersion] of Object.entries(dependencies)) {
        const comparison = await compareVersions(file, minVersion);
        if (comparison < 0) {
            throw new Error(`${file} is not installed or older than min-version ${minVersion}. please install/update först and retry`);
        }
    }
}
exports.ensureDependencies = ensureDependencies;
function versionUtility() {
    return {
        parseVersion,
        compareVersions,
        ensureDependencies,
    };
}
exports.default = versionUtility;
