import { Cli } from './Cli';
import commander from 'commander';
export declare const DEFAULT_INCLUDE_PATH: string[];
export declare type CommandFactory = (program: commander.Command, cli: Cli) => unknown;
export declare type Commands = CommandFactory[];
export interface CommandsRegistry {
    commands: Commands;
    load(includePaths: string | string[]): Promise<CommandsRegistry>;
}
export default function commandsRegistry(includePaths?: string | string[], commands?: Commands): Promise<CommandsRegistry>;
