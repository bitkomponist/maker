import { SpawnArguments } from './ChildProcessUtility';
export declare function parseVersion(cmd: string, flag?: string, ...args: SpawnArguments): Promise<boolean | string>;
export declare function compareVersions(cmd: string, targetVersion: string, flag?: string, ...args: SpawnArguments): Promise<0 | 1 | -1>;
export declare function ensureDependencies(dependencies: Record<string, string>): Promise<void>;
export interface VersionUtility {
    parseVersion(cmd: string, flag?: string, ...args: SpawnArguments): Promise<boolean | string>;
    compareVersions(cmd: string, targetVersion: string, flag?: string, ...args: SpawnArguments): Promise<0 | 1 | -1>;
    ensureDependencies(dependencies: Record<string, string>): void;
}
export default function versionUtility(): VersionUtility;
