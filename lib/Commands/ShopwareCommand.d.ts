import commander from 'commander';
import { Cli } from '../Cli';
declare const _default: (programm: commander.Command, cli: Cli) => void;
export default _default;
