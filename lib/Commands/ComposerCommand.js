"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// module.exports = class ComposerCommand {
//   static name = 'composer';
//   static description =
//     'invoke composer in the configured application directory on the web container';
//   constructor(cli) {
//     this.cli = cli;
//   }
//   async getWD() {
//     const config = await this.cli.getConfigFile();
//     return `/var/www/html/${config.data.applicationDirectory || ''}`;
//   }
//   async run() {
//     const { cli } = this;
//     const composerArgs = cli.argv.slice(1);
//     const wd = await this.getWD();
//     await cli.spawnWithIO('ddev', [
//       'exec',
//       'composer',
//       `--working-dir=${wd}`,
//       ...composerArgs,
//     ]);
//   }
// }
exports.default = (programm, cli) => {
    programm
        .command('composer [...composer-args]')
        .description('invokes composer in the configured app directory on the web container')
        .action(async (_arg0, { args }) => {
        const config = await cli.getConfigFile();
        const wd = `/var/www/html/${config.get('applicationDirectory', '')}`;
        await cli.spawnWithIO('ddev', [
            'exec',
            'composer',
            `--working-dir=${wd}`,
            ...args,
        ]);
    });
};
