"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = (programm, cli) => {
    programm
        .command('exec <argsString>')
        .usage('maker exec "ls -lh"')
        .description('exec commands, for dev purposes')
        .action(async (argsString) => {
        const [cmdFile, ...cmdArgs] = argsString.split(' ');
        await cli.spawnWithIO(cmdFile, cmdArgs);
    });
};
