"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.execWithIO = exports.exec = exports.spawnWithIO = exports.spawn = void 0;
const child_process_1 = require("child_process");
function spawn(cmd, args = [], options = {}, createdCallback) {
    options = {
        encoding: 'utf8',
        maxBuffer: 1024 * 1024 * 100,
        ...(options || {}),
    };
    return new Promise((resolve, reject) => {
        const childProcess = child_process_1.spawn(cmd, args, options);
        if (createdCallback) {
            createdCallback(childProcess);
        }
        let result = '';
        let err = '';
        const onOutData = (chunk) => (result += chunk);
        const onErrorData = (chunk) => (err += chunk);
        const stdout = childProcess.stdout || process.stdout;
        const stderr = childProcess.stderr || process.stderr;
        const stdin = childProcess.stdin || process.stdin;
        stdout.on('data', onOutData);
        stderr.on('data', onErrorData);
        childProcess.on('error', (error) => {
            stdout.off('data', onOutData);
            stderr.off('data', onErrorData);
            reject(error);
        });
        childProcess.on('close', (code) => {
            stdout.off('data', onOutData);
            stderr.off('data', onErrorData);
            stdin.end();
            if (code) {
                reject(err.trim());
            }
            else {
                resolve(result.trim());
            }
        });
        // childProcess.on('exit', (code) => {
        //   stdout.off('data', onOutData);
        //   stderr.off('data', onErrorData);
        //   if (code) {
        //     reject(err.trim());
        //   } else {
        //     resolve(result.trim());
        //   }
        // });
        /*
          const spawn = require('child_process').spawn;
          const bat = spawn('ssh', ['user@host']);
    
          bat.stdout.on('data', function(d) {
            console.log(d.toString());
          });
          bat.stderr.on('data', function(d) {
            console.log(d.toString());
          });
    
          bat.on('error', function(error) {
            console.log('child err\'d with : ' + error);
          });
          bat.on('close', function(code, signal) {
            console.log('child exited with code: ' + code + ' and signal: ' + signal);
          });
    
          process.stdin.on('readable', () => {
            var chunk = process.stdin.read();
            if (chunk !== null) {
              bat.stdin.write(chunk);
            }
          });
        */
    });
}
exports.spawn = spawn;
function spawnWithIO(cmd, args = [], options = {}, createdCallback) {
    return spawn(cmd, args, { stdio: 'inherit', env: process.env, ...options }, createdCallback);
}
exports.spawnWithIO = spawnWithIO;
function exec(cmd, options = {}, createdCallback) {
    const args = cmd.split(' ');
    const file = args.shift();
    if (!file) {
        throw new Error(`invalid command`);
    }
    return spawn(file, args, options, createdCallback);
}
exports.exec = exec;
function execWithIO(cmd, options = {}, createdCallback) {
    return exec(cmd, { stdio: 'inherit', env: process.env, ...options }, createdCallback);
}
exports.execWithIO = execWithIO;
function childProcessUtility() {
    return {
        spawn,
        spawnWithIO,
        exec,
        execWithIO,
    };
}
exports.default = childProcessUtility;
