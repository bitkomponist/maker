export declare function style(message: string, ...styles: string[]): string;
export declare function info(message: unknown, ...styles: string[]): void;
export declare function warn(message: unknown, ...styles: string[]): void;
export declare function error(message: unknown, ...styles: string[]): void;
export declare function success(message: unknown, ...styles: string[]): void;
export interface Logger {
    style(message: string, ...styles: string[]): string;
    log(message: unknown, ...styles: string[]): void;
    info(message: unknown, ...styles: string[]): void;
    warn(message: unknown, ...styles: string[]): void;
    error(message: unknown, ...styles: string[]): void;
    success(message: unknown, ...styles: string[]): void;
}
export default function logger(): Logger;
