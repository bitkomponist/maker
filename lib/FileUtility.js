"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getConfigFile = exports.getJsonFile = exports.getFile = exports.ConfigFile = exports.JsonFile = exports.File = void 0;
const fs_1 = require("fs");
const path_1 = require("path");
const lodash_get_1 = __importDefault(require("lodash.get"));
const lodash_set_1 = __importDefault(require("lodash.set"));
class File {
    constructor(path, transform, serialize) {
        this.path = '';
        this.data = '';
        Object.assign(this, { path, transform, serialize });
    }
    async load() {
        const { transform, path } = this;
        if (fs_1.existsSync(path)) {
            this.data = await fs_1.promises.readFile(path, { encoding: 'utf8' });
        }
        if (transform && typeof this.data === 'string') {
            this.data = transform(this.data);
        }
        return this;
    }
    async save() {
        const { path, data, serialize = String } = this;
        if (!fs_1.existsSync(path)) {
            await this.ensurePath();
        }
        await fs_1.promises.writeFile(path, serialize(data), { encoding: 'utf8' });
        return this;
    }
    ensurePath() {
        return fs_1.promises.mkdir(path_1.dirname(this.path), { recursive: true });
    }
}
exports.File = File;
class JsonFile extends File {
    constructor(path) {
        super(path, (str) => {
            if (str === '')
                return {};
            try {
                return JSON.parse(str);
            }
            catch (e) {
                console.warn(e);
                return {};
            }
        }, (obj) => JSON.stringify(obj, null, 4));
        this.data = {};
    }
    async load() {
        await super.load();
        if (!this.data) {
            this.data = {};
        }
        return this;
    }
    get(path, defaultValue) {
        if (!this.data) {
            this.data = {};
        }
        return lodash_get_1.default(this.data, path, defaultValue);
    }
    set(path, value) {
        if (!this.data) {
            this.data = {};
        }
        return lodash_set_1.default(this.data, path, value);
    }
}
exports.JsonFile = JsonFile;
class ConfigFile extends JsonFile {
    constructor(path) {
        if (!path) {
            path = path_1.join(process.cwd(), '.maker', 'config.json');
        }
        super(path);
    }
}
exports.ConfigFile = ConfigFile;
function getFile(path, transform, serialize) {
    return new File(path, transform, serialize).load();
}
exports.getFile = getFile;
function getJsonFile(path) {
    return new JsonFile(path).load();
}
exports.getJsonFile = getJsonFile;
function getConfigFile(path) {
    return new ConfigFile(path).load();
}
exports.getConfigFile = getConfigFile;
function fileUtility() {
    return {
        getFile,
        getJsonFile,
        getConfigFile,
    };
}
exports.default = fileUtility;
