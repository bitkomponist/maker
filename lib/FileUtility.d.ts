import { Json } from './Json';
import { PropertyPath } from 'lodash';
export declare type FileDataTransformer = (serialized: string) => unknown;
export declare type FileDataSerializer = (transformed: unknown) => string;
export declare class File {
    protected path: string;
    protected transform?: (contents: string) => unknown;
    protected serialize?: (transformed: unknown) => string;
    data?: unknown;
    constructor(path: string, transform?: FileDataTransformer, serialize?: FileDataSerializer);
    load(): Promise<this>;
    save(): Promise<this>;
    ensurePath(): Promise<string>;
}
export declare class JsonFile extends File {
    data: Json;
    constructor(path: string);
    load(): Promise<this>;
    get(path: PropertyPath, defaultValue?: unknown): any;
    set(path: PropertyPath, value: unknown): Json;
}
export declare class ConfigFile extends JsonFile {
    constructor(path?: string);
}
export declare function getFile(path: string, transform?: FileDataTransformer, serialize?: FileDataSerializer): Promise<File>;
export declare function getJsonFile(path: string): Promise<JsonFile>;
export declare function getConfigFile(path?: string): Promise<ConfigFile>;
export interface FileUtility {
    getFile(path: string, transform?: FileDataTransformer, serialize?: FileDataSerializer): Promise<File>;
    getJsonFile(path: string): Promise<JsonFile>;
    getConfigFile(path?: string): Promise<ConfigFile>;
}
export default function fileUtility(): FileUtility;
