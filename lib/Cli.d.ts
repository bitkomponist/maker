import { SpawnArguments, ChildProcessUtility } from './ChildProcessUtility';
import { Logger } from './Logger';
import { FileUtility } from './FileUtility';
import { VersionUtility } from './VersionUtility';
import { CommandsRegistry, Commands } from './CommandsRegistry';
export interface Cli extends Logger, FileUtility, ChildProcessUtility, VersionUtility, CommandsRegistry {
    run(...args: SpawnArguments): Promise<unknown>;
}
export default function cli(includePaths?: string | string[], commands?: Commands): Promise<Cli>;
