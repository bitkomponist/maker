"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.success = exports.error = exports.warn = exports.info = exports.style = void 0;
const CONSOLE_STYLES = {
    reset: '\x1b[0m',
    bright: '\x1b[1m',
    dim: '\x1b[2m',
    underscore: '\x1b[4m',
    blink: '\x1b[5m',
    reverse: '\x1b[7m',
    hidden: '\x1b[8m',
    black: '\x1b[30m',
    red: '\x1b[31m',
    green: '\x1b[32m',
    yellow: '\x1b[33m',
    blue: '\x1b[34m',
    magenta: '\x1b[35m',
    cyan: '\x1b[36m',
    white: '\x1b[37m',
    'bg-black': '\x1b[40m',
    'bg-red': '\x1b[41m',
    'bg-green': '\x1b[42m',
    'bg-yellow': '\x1b[43m',
    'bg-blue': '\x1b[44m',
    'bg-magenta': '\x1b[45m',
    'bg-cyan': '\x1b[46m',
    'bg-white': '\x1b[47m',
};
function getConsoleStyle(...styles) {
    return [
        ...styles.map((key) => CONSOLE_STYLES[key]).filter(Boolean),
        '%s',
        CONSOLE_STYLES.reset,
    ].join('');
}
function log(message, ...styles) {
    if (!styles.length) {
        console.log(message);
    }
    else {
        console.log(getConsoleStyle(...styles), message);
    }
}
function style(message, ...styles) {
    return getConsoleStyle(...styles).replace('%s', message);
}
exports.style = style;
function info(message, ...styles) {
    log(message, 'cyan', ...styles);
}
exports.info = info;
function warn(message, ...styles) {
    log(message, 'yellow', ...styles);
}
exports.warn = warn;
function error(message, ...styles) {
    log(message, 'red', ...styles);
}
exports.error = error;
function success(message, ...styles) {
    log(message, 'green', ...styles);
}
exports.success = success;
function logger() {
    return {
        log,
        style,
        info,
        warn,
        error,
        success,
    };
}
exports.default = logger;
