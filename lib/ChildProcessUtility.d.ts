/// <reference types="node" />
import { ChildProcess } from 'child_process';
export declare type SpawnedCallback = (c: ChildProcess) => void;
export declare type SpawnArguments = readonly string[];
export declare function spawn(cmd: string, args?: SpawnArguments, options?: Record<string, unknown>, createdCallback?: SpawnedCallback): Promise<string>;
export declare function spawnWithIO(cmd: string, args?: SpawnArguments, options?: {}, createdCallback?: SpawnedCallback): Promise<string>;
export declare function exec(cmd: string, options?: {}, createdCallback?: SpawnedCallback): Promise<string>;
export declare function execWithIO(cmd: string, options?: {}, createdCallback?: SpawnedCallback): Promise<string>;
export interface ChildProcessUtility {
    spawn(cmd: string, args: SpawnArguments, options?: Record<string, unknown>, createdCallback?: SpawnedCallback): Promise<string>;
    spawnWithIO(cmd: string, args: SpawnArguments, options?: Record<string, unknown>, createdCallback?: SpawnedCallback): Promise<string>;
    exec(cmd: string, options?: Record<string, unknown>, createdCallback?: SpawnedCallback): Promise<string>;
    execWithIO(cmd: string, options?: Record<string, unknown>, createdCallback?: SpawnedCallback): Promise<string>;
}
export default function childProcessUtility(): ChildProcessUtility;
