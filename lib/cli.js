"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ChildProcessUtility_1 = __importDefault(require("./ChildProcessUtility"));
const Logger_1 = __importDefault(require("./Logger"));
const FileUtility_1 = __importDefault(require("./FileUtility"));
const VersionUtility_1 = __importDefault(require("./VersionUtility"));
const CommandsRegistry_1 = __importDefault(require("./CommandsRegistry"));
const commander_1 = __importDefault(require("commander"));
async function cli(includePaths, commands = []) {
    const registry = await CommandsRegistry_1.default(includePaths, commands);
    const programm = new commander_1.default.Command();
    const instance = {
        ...registry,
        ...Logger_1.default(),
        ...ChildProcessUtility_1.default(),
        ...FileUtility_1.default(),
        ...VersionUtility_1.default(),
        async run(...argv) {
            await programm.parseAsync([...argv]);
            return instance;
        },
    };
    for (const commandFactory of registry.commands) {
        commandFactory(programm, instance);
    }
    return instance;
}
exports.default = cli;
