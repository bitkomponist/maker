"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DEFAULT_INCLUDE_PATH = void 0;
const path_1 = require("path");
const fast_glob_1 = __importDefault(require("fast-glob"));
exports.DEFAULT_INCLUDE_PATH = [
    path_1.join(__dirname, 'commands/*.js'),
    '.maker/commands/*.js',
];
async function commandsRegistry(includePaths = exports.DEFAULT_INCLUDE_PATH, commands = []) {
    async function load(includePaths) {
        for (let file of await fast_glob_1.default(includePaths)) {
            if (!file.startsWith(path_1.sep)) {
                file = path_1.join(process.cwd(), file);
            }
            const { default: commandFactory } = await Promise.resolve().then(() => __importStar(require(file)));
            if (typeof commandFactory !== 'function')
                throw new Error(`invalid CommandFactory ${file}`);
            commands.push(commandFactory);
        }
        return this;
    }
    const registry = {
        commands,
        load,
    };
    return registry.load(includePaths);
}
exports.default = commandsRegistry;
