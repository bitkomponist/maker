export declare type Json = null | boolean | number | string | Json[] | {
    [prop: string]: Json;
};
export declare type JsonCompatible<T> = {
    [P in keyof T]: T[P] extends Json ? T[P] : Pick<T, P> extends Required<Pick<T, P>> ? never : T[P] extends (() => unknown) | undefined ? never : JsonCompatible<T[P]>;
};
